# Vulkan files

Documents from the "Vulkan leak" both in russin (original) and english (translated).

- Amezit: blueprint for surveilling and controlling the internet in regions under Russia’s command, and also enables disinformation via fake social media profiles
- Crystal-2V: training program for cyber-operatives in the methods required to bring down rail, air and sea infrastructure
- Sandworm: “data exchange protocol” between a database containing intelligence about software and hardware weaknesses, and Scan-V
- Scan-V: conduct automated reconnaissance of potential targets around the world in a hunt for potentially vulnerable servers and network devices

More info: https://www.theguardian.com/technology/2023/mar/30/vulkan-files-leak-reveals-putins-global-and-domestic-cyberwarfare-tactics
